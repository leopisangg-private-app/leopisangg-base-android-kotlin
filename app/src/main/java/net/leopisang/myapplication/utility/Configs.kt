package net.leopisang.myapplication.utility

import net.leopisang.myapplication.BuildConfig

/**
 * Created by LeoPisanGG on 2018-02-17.
 */
class Configs {
    companion object {
        fun BUILD_URL(URL:String): String = BuildConfig.BASE_URL + URL
    }
}